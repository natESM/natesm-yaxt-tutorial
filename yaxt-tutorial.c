/* execute the program with 15 tasks */

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <mpi.h>
#include <yaxt.h>

// #define VERBOSE
enum {
  // define GRID size
  GRID_COLUMN = 10,
  GRID_ROW = 10,
  GRID_LEVEL = 10,
  PROCS = 15,
  LOOPS = 10
};

static void partition_1D(int global_count, int comm_rank, int comm_size, int * start_idx, int * len);
static void data_initialization(bool is_step1, int rank_split, int size_split, int *index_begin, int *index_len, int **ptr);
static void data_initialization_step1(int rank_split, int size_split, int *index_begin, int *index_len, int **ptr_data);
static void data_initialization_step2(int rank_split, int size_split, int *index_begin, int *index_len, int **ptr_data);
static void main_step1(MPI_Comm comm_split);
static void main_step2(MPI_Comm comm_split);
static void *xcalloc(size_t count, size_t size, const char *source, int line);
static void increment(const int level, const int row, const int column, int *const ptr_data, int increment);

#define XCALLOC(count,size) \
  xcalloc((count), (size), __FILE__, __LINE__)


int main(int argc, char* argv[]) {
  int size, rank;
  xt_mpi_call(MPI_Init(NULL, NULL), MPI_COMM_WORLD);
  xt_initialize(MPI_COMM_WORLD);
  xt_mpi_call(MPI_Comm_size(MPI_COMM_WORLD, &size), MPI_COMM_WORLD);
  xt_mpi_call(MPI_Comm_rank(MPI_COMM_WORLD, &rank), MPI_COMM_WORLD);

  if (size != PROCS) {
    printf("Abort! Number of MPI tasks is %d, Number of MPI tasks must be %d!\n", size, PROCS);
    MPI_Finalize();
    exit(EXIT_FAILURE);
  }

  // split process into two groups
  // color = 0: 2/3 (10)processes for step 1 simulation
  // color = 1: 1/3 (5) processes for step 2 simulation
  int color = 0;
  MPI_Comm comm_split;
  int size_split, rank_split;
  if (rank % 3 == 2) color = 1;
  xt_mpi_call(MPI_Comm_split(MPI_COMM_WORLD, color, rank, &comm_split), MPI_COMM_WORLD);
  if (color == 0) {
    main_step1(comm_split);
  } else {
    main_step2(comm_split);
  }
  if (rank == 0)
    printf("test passed!\n");
  xt_mpi_call(MPI_Comm_free(&comm_split), MPI_COMM_WORLD);
  xt_finalize();
  MPI_Finalize();
  return 0;
}

void main_step1(MPI_Comm comm_split) {
  int size_split, rank_split;
  xt_mpi_call(MPI_Comm_size(comm_split, &size_split), MPI_COMM_WORLD);
  xt_mpi_call(MPI_Comm_rank(comm_split, &rank_split), MPI_COMM_WORLD);

  int *ptr_data = NULL;
  int *ptr_ref = NULL;
  int index_begin = 0, index_len = 0;
  // domain decomposition and fill the initial data in step 1
  data_initialization(true, rank_split, size_split, &index_begin, &index_len, &ptr_data);
  // initialise the reference data the same as ptr_data
  ptr_ref = XCALLOC(index_len * GRID_ROW * GRID_COLUMN, sizeof(*ptr_ref));
  for (int k = 0; k < index_len; ++k)
    for (int i = 0; i < GRID_ROW; ++i)
      for (int j = 0; j < GRID_COLUMN; ++j) {
        size_t index = k * GRID_ROW * GRID_COLUMN + i * GRID_COLUMN + j;
        ptr_ref[index] = ptr_data[index];
      }

  // 1. create idxlist
  // please create an index section to describe the decomposition with xt_idxsection_new()
  // Xt_idxlist xt_idxsection_new(Xt_int start, int num_dimensions,
  //                              const Xt_int global_size[num_dimensions],
  //                              const int local_size[num_dimensions],
  //                              const Xt_int local_start[num_dimensions]);
  // Parameters
  // [in]	start	          lowest index of the global array (typically 0 or 1)
  // [in]	num_dimensions	number of dimensions
  // [in]	global_size	    global size of each dimension
  // [in]	local_size	    size of the local section in each dimension
  // [in]	local_start	    vector with the lowest position in each dimension of the local window within the global index space
  Xt_int start = 0;
  int num_dimensions = 3;
  Xt_int global_size[3] = {GRID_LEVEL, GRID_ROW, GRID_COLUMN};
  // create step1 index list, step2 index list is empty
  int local_size_step1[3] = {____, ____, ____};
  Xt_int local_start_step1[3] = {____, ____, ____};
  Xt_idxlist idxsection_step1 \
    = xt_idxsection_new(start, num_dimensions, global_size,
        local_size_step1, local_start_step1);
  // plase find a proper function to create step2 index list
  Xt_idxlist idxsection_step2 = ____();

  // 2. generate xmap
  // please call xt_xmap_all2all_new to alculate abstract communication pattern
  // between step1 and step2 index list
  // Xt_xmap xt_xmap_all2all_new	(	Xt_idxlist 	src_idxlist,
  //                                Xt_idxlist 	dst_idxlist,
  //                                MPI_Comm 	comm 
  //                              )	
  // Parameters
  // [in]	src_idxlist	     source index list
  // [in]	dst_idxlist	     destination index list
  // [in]	comm	           MPI communicator that contains all processes that take part in the exchange (xt_xmap_all2all_new will make its own copy of comm)
  // xmap_step2_to_step1: step2 array => step 1 array, xmap_step1_to_step2: step1 array => step2 array
  Xt_xmap xmap_step2_to_step1 = xt_xmap_all2all_new(____, ____, MPI_COMM_WORLD);
  Xt_xmap xmap_step1_to_step2 = xt_xmap_all2all_new(____, ____, MPI_COMM_WORLD);

  // 3. generate redistribution object
  // please call xt_redist_p2p_new to create a redistribution object
  // Xt_redist xt_redist_p2p_new	(	Xt_xmap 	xmap,
  //                                MPI_Datatype 	datatype 
  //                              )
  // Parameters
  // [in]	xmap	       exchange map
  // [in]	datatype	   MPI datatype of single element in the data to be exchanged
  Xt_redist redist_step2_to_step1 = xt_redist_p2p_new(xmap_step2_to_step1, ____);
  Xt_redist redist_step1_to_step2 = xt_redist_p2p_new(xmap_step1_to_step2, ____);

  for (int l = 0; l < LOOPS; ++l) {
    // YAXT data exchange
    // void xt_exchanger_s_exchange	(	Xt_exchanger 	exchanger,
    //                                const void * 	src_data,
    //                                void * 	dst_data 
    //                              )	
    // Executes a synchronous data exchange.
    // Parameters
    // [in]	  exchanger 	exchanger object
    // [in]	  src_data	  source data
    // [out]	dst_data	  destination data
    // exchange data from step1 to step2
    xt_redist_s_exchange1(____, ptr_data, NULL);
    // exchange data from step2 to step1
    xt_redist_s_exchange1(____, NULL, ptr_data);
    // verfify the data after yaxt exchange
    for (int k = 0; k < index_len; ++k)
      for (int i = 0; i < GRID_ROW; ++i)
        for (int j = 0; j < GRID_COLUMN; ++j) {
          size_t index = k * GRID_ROW * GRID_COLUMN + i * GRID_COLUMN + j;
          if (ptr_data[index] != ptr_ref[index] + l) {
            fprintf(stderr, "VERIFICATION FAILED!! color: 0, loop: %d, step1value: %d, step1verify: %d, element_index: [%d, %d, %d], split rank: %d \n",
                    l, ptr_data[index], ptr_ref[index], k, i, j, rank_split);
            exit(EXIT_FAILURE);
          }
          ptr_data[index] = ptr_ref[index];
        }
    // print array after yaxt exchange in step1, this value should be the same as step1's initial value
#ifdef VERBOSE
    for (int k = 0; k < index_len; ++k)
      for (int i = 0; i < GRID_ROW; ++i)
        for (int j = 0; j < GRID_COLUMN; ++j)
        {
          fprintf(stderr, "color: 0, loop: %d, step1value: %d, element_index: [%d, %d, %d], split rank: %d \n",
                  l, ptr_data[k * GRID_ROW * GRID_COLUMN + i * GRID_COLUMN + j], k, i, j, rank_split);
        }
#endif
  }

  free(ptr_data);
  free(ptr_ref);
  xt_redist_delete(redist_step2_to_step1);
  xt_redist_delete(redist_step1_to_step2);
  xt_xmap_delete(xmap_step2_to_step1);
  xt_xmap_delete(xmap_step1_to_step2);
  xt_idxlist_delete(idxsection_step1);
  xt_idxlist_delete(idxsection_step2);
}

void main_step2(MPI_Comm comm_split) {
  int size_split, rank_split;
  xt_mpi_call(MPI_Comm_size(comm_split, &size_split), MPI_COMM_WORLD);
  xt_mpi_call(MPI_Comm_rank(comm_split, &rank_split), MPI_COMM_WORLD);

  int *ptr_data = NULL;
  int index_begin = 0, index_len = 0;
  // partition the matrix along grid_column dimension
  data_initialization(false, rank_split, size_split, &index_begin, &index_len, &ptr_data); 

  // 1. create idxlist
  // please create an index section to describe the decomposition with xt_idxsection_new()
  // Xt_idxlist xt_idxsection_new(Xt_int start, int num_dimensions,
  //                              const Xt_int global_size[num_dimensions],
  //                              const int local_size[num_dimensions],
  //                              const Xt_int local_start[num_dimensions]);
  // Parameters
  // [in]	start	          lowest index of the global array (typically 0 or 1)
  // [in]	num_dimensions	number of dimensions
  // [in]	global_size	    global size of each dimension
  // [in]	local_size	    size of the local section in each dimension
  // [in]	local_start	    vector with the lowest position in each dimension of the local window within the global index space
  Xt_int start = 0;
  int num_dimensions = 3;
  Xt_int global_size[3] = {GRID_LEVEL, GRID_ROW, GRID_COLUMN};
  int local_size_step2[3] = {____, ____, ____};
  Xt_int local_start_step2[3] = {____, ____, ____};
  // plase find a proper function to create step1 index list
  Xt_idxlist idxsection_step1 = ____();
  Xt_idxlist idxsection_step2 \
    = xt_idxsection_new(start, num_dimensions, global_size,
        local_size_step2, local_start_step2);

  // 2. generate xmap
  // please call xt_xmap_all2all_new to alculate abstract communication pattern
  // between step1 and step2 index list
  // Xt_xmap xt_xmap_all2all_new	(	Xt_idxlist 	src_idxlist,
  //                                Xt_idxlist 	dst_idxlist,
  //                                MPI_Comm 	comm 
  //                              )	
  // Parameters
  // [in]	src_idxlist	     source index list
  // [in]	dst_idxlist	     destination index list
  // [in]	comm	           MPI communicator that contains all processes that take part in the exchange (xt_xmap_all2all_new will make its own copy of comm)
  // xmap_step2_to_step1: step2 array => step 1 array, xmap_step1_to_step2: step1 array => step2 array 
  Xt_xmap xmap_step2_to_step1 = xt_xmap_all2all_new(____, ____, MPI_COMM_WORLD);
  Xt_xmap xmap_step1_to_step2 = xt_xmap_all2all_new(____, ____, MPI_COMM_WORLD);

  // 3. generate redistribution object
  // please call xt_redist_p2p_new to create a redistribution object
  // Xt_redist xt_redist_p2p_new	(	Xt_xmap 	xmap,
  //                                MPI_Datatype 	datatype 
  //                              )
  // Parameters
  // [in]	xmap	       exchange map
  // [in]	datatype	   MPI datatype of single element in the data to be exchanged
  Xt_redist redist_step2_to_step1 = xt_redist_p2p_new(xmap_step2_to_step1, ____);
  Xt_redist redist_step1_to_step2 = xt_redist_p2p_new(xmap_step1_to_step2, ____);

  for (int l = 0; l < LOOPS; ++l) {
    // YAXT data exchange
    // void xt_exchanger_s_exchange	(	Xt_redist redist,
    //                                const void * 	src_data,
    //                                void * 	dst_data 
    //                              )	
    // Executes a synchronous data exchange.
    // Parameters
    // [in]	  redist    	redistribution object
    // [in]	  src_data	  source data
    // [out]	dst_data	  destination data
    // exchange data from step1 to step2
    xt_redist_s_exchange1(____, NULL, ptr_data);
    // increment each value of ptr_data by l, loop index
    increment(GRID_LEVEL, GRID_ROW, index_len, ptr_data, l);
#ifdef VERBOSE
    // print array after yaxt exchange in step2
    for (int k = 0; k < GRID_LEVEL; ++k)
      for (int i = 0; i < GRID_ROW; ++i)
        for (int j = 0; j < index_len; ++j)
          fprintf(stderr, "color: 1, loop: %d, step2value: %d, element_index: [%d, %d, %d], split rank: %d \n",
              l, ptr_data[k*GRID_ROW*index_len + i * index_len + j], k, i, j, rank_split);
#endif
    // exchange data from step2 to step1
    xt_redist_s_exchange1(_____, ptr_data, NULL);
  }

  free(ptr_data);
  xt_redist_delete(redist_step2_to_step1);
  xt_redist_delete(redist_step1_to_step2);
  xt_xmap_delete(xmap_step2_to_step1);
  xt_xmap_delete(xmap_step1_to_step2);
  xt_idxlist_delete(idxsection_step1);
  xt_idxlist_delete(idxsection_step2);
}

/**
  data decomposition(step 1 & 2) and initialization(step 1)
  @param[in]  is_step1     true: step1 data initialization, false: step2 data initialization
  @param[in]  rank_split   MPI rank in this step
  @param[in]  size_split   MPI size in this step
  @param[out] index_begin  beginning index of the dimension being partitioned
  @param[out] index_len    length of the dimension being partitioned
  @param[out] ptr          pointer to data, if is_step1 is true, then data is initialised
*/
void data_initialization(bool is_step1, int rank_split, int size_split, int *index_begin, int *index_len, int **ptr) {
  if (is_step1) {
    data_initialization_step1(rank_split, size_split, index_begin, index_len, ptr);
  } else {
    data_initialization_step2(rank_split, size_split, index_begin, index_len, ptr);
  }
}

void data_initialization_step1(int rank_split, int size_split, int *index_begin, int *index_len, int **ptr_data) {
  partition_1D(GRID_LEVEL, rank_split, size_split, index_begin, index_len);
#ifdef VERBOSE
  fprintf(stderr, "color: 0, split rank: %d, split size: %d, index_begin: %d, index_len: %d.\n",
          rank_split, size_split, *index_begin, *index_len);
#endif
  // initialize local grids in step1
  *ptr_data = XCALLOC((*index_len) * GRID_ROW * GRID_COLUMN, sizeof(**ptr_data));
  if (*ptr_data == NULL)
  {
    printf("Abort! Failed to alloc step1 grid matrix.\n");
    exit(EXIT_FAILURE);
  }
  for (int k = 0; k < (*index_len); ++k)
    for (int i = 0; i < GRID_ROW; ++i)
      for (int j = 0; j < GRID_COLUMN; ++j)
      {
        // 8 digits number
        // 1st position = 1,
        // 2nd-3rd position level, 4th-5th positon row number
        // 6th-7th position column number
        (*ptr_data)[k * GRID_ROW * GRID_COLUMN + i * GRID_COLUMN + j] =
            10000000 + 100000 * (k + (*index_begin)) + 1000 * i + 10 * j;
#ifndef VERBOSE
      }
#else
        if (rank_split == 2) {
          // print initial array in step1
          fprintf(stderr, "init step1value: %d, element_index: [%d, %d, %d], rank: %d.\n",
                  (*ptr_data)[k * GRID_ROW * GRID_COLUMN + i * GRID_COLUMN + j], k, i, j, rank_split);
        }
      }
#endif
}

void data_initialization_step2(int rank_split, int size_split, int *index_begin, int *index_len, int **ptr_data) {
  partition_1D(GRID_COLUMN, rank_split, size_split, index_begin, index_len);
  // initialize local grids in step2
  *ptr_data = XCALLOC(GRID_LEVEL * GRID_ROW * (*index_len), sizeof(**ptr_data));
  if (*ptr_data == NULL)
  {
    printf("Abort! Failed to alloc step2 grid matrix.\n");
    exit(EXIT_FAILURE);
  }
#ifdef VERBOSE
  fprintf(stderr, "color: 1, split rank: %d, split size: %d, index_begin: %d, index_len: %d.\n",
          rank_split, size_split, *index_begin, *index_len);
#endif
}

void increment(const int level, const int row, const int column, int *const ptr_data, int increment) {
  // increment each value of ptr_data by 1
  for (int k = 0; k < level; ++k)
      for (int i = 0; i < row; ++i)
        for (int j = 0; j < column; ++j)
          ptr_data[k*row*column + i * column + j] += increment;
}

/**
  Partitions a global count into "size" parts as evenly as possible
  @param[in]  global_count global count to be distributed
  @param[in]  idx          index of the local partition (0 <= idx < size)
  @param[in]  size         total number of parts
  @param[out] start_idx    start index of local partition (0 <= start_idx < global_count)
  @param[out] len          number of elements in local parition (1 <= len <= global_count)
*/
void partition_1D(int global_count, int comm_rank, int comm_size, int *start_idx, int *len) {
  *start_idx = (int)((long)global_count * (long)comm_rank / (long)comm_size);
  *len = (int)(((long)global_count * (long)(comm_rank+1)) / (long)comm_size) - *start_idx;
}

/**
  allocate with error check
  @param[in]  count   number of elements
  @param[in]  size    size of each element in byte
  @param[out] source  file name of this function being called
  @param[out] line    line of this function being called
*/
void *xcalloc(size_t count, size_t size, const char *source, int line) {
  void *p = calloc(count, size);
  if (!p && count && size) {
    fprintf(stderr, "Abort! %s:%d, failed to dynamically alloc matrix.\n", source, line);
    exit(EXIT_FAILURE);
  }
  return p;
}