include Make.inc


EXEC = yaxt-tutorial
OBJ =


all: $(EXEC)

yaxt-tutorial: $(OBJ) yaxt-tutorial.o
	$(CC) $(LDFLAGS) -o $@ yaxt-tutorial.o $(LIBS)

clean:
	$(RM) -rf $(EXEC) *.o *.mod core*

.SUFFIXES:
.SUFFIXES: .F90 .f90 .c .o

.f90.o :
	$(F90) $(INCLUDES) $(F90FLAGS) -c $< -o $@

.F90.o :
	$(F90) $(FPPFLAGS) $(INCLUDES) $(F90FLAGS) -c $< -o $@

.c.o :
	$(CC) $(INCLUDES) $(CFLAGS) -c $< -o $@
