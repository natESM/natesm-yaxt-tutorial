YAXT_ROOT = /work/k20200/k202149/sw/yaxt_v0.9.3.1_icc_ompi
YAXT_LIB = $(YAXT_ROOT)/lib/libyaxt.a $(YAXT_ROOT)/lib/libyaxt_c.a
YAXT_INCLUDE = -I$(YAXT_ROOT)/include

INCLUDES = $(YAXT_INCLUDE)
LIBS = $(YAXT_LIB)

F90FLAGS = -O0 -g
FPPFLAGS = -fpp
FLDFLAGS =

CFLAGS = -O0 -g -Wall
LDFLAGS =

F90 = mpif90
CC = mpicc
