# YAXT Tutorial
You have a simple model with cubic shape. It uses a 10x10 regular horizontal grid and has 10 vertical levels. The model has a single field, which is updated by two simulation steps:

  * step 1 uses a vertical decomposition --> the levels of the global grid are equally distributed among the processes
  * step 2 uses a horizontal decomposition --> the columns of the global grid are evenly distributed among the processes.

Step 1 and step 2 access the same grid but use different decompositions. Your model runs in parallel on 15 MPI processes: 10 MPI processes run the step 1(e.g. each MPI process has 1 level) and 5 MPI processes run step 2(e.g. each MPI process has 2 columns).

The step 1 first initialises its data, while step 2 gets its initial data from step 1. Afterwards, step 2 computes the data it receives and sends back to step 1. This exchange repeats 10 times.
The number in level 5, row 4, column 6 is initialised as 10504060

```
1  0  5  0  4  0  6  0
   |__|  |__|  |__|
   level  row  column
```
The two steps run after each other in a time loop.

Since both steps have a different domain decomposition, a transpose operation has to occur between the steps, which redistribute the data of the field from one decomposition to the other.

step 1 verifies the data it receives from step 2 with reference data. If the verification is successful, print out "test passed!"

Please fill the blanks "____" in `yaxt-tutorial.c`

![program flow](figure-1.png)

## Compilation
```
# load compilers and MPI modules first
$ module load intel-oneapi-compilers/2022.0.1-gcc-11.2.0
$ module load openmpi/4.1.2-intel-2021.5.0

# build 
$ make

# clean the build
$ make clean
```

## Run
There are two options to run the executable

1. 
```
# submit the experiment
# !! please remember to change #SBATCH --account= to your individual account on Levante !!
$ sbatch exp.yaxt-tutorial.run

# check the results
$ vi LOG.yaxt-tutorial.xxxx.o # XXXX is a SLURM job number
```
2. 
```
# create an interactive node
$ salloc -p compute -A YYYY -t 02:00:00 --reservation=natESM -N 1

# setup the environment
$ export OMPI_MCA_btl="^vader,tcp,openib,smcuda"

# run the experiment with 15 MPI tasks
$ srun -n 15 yaxt-tutorial
```

## Reference
during the tutorial you may want to check the following information:
* YAXT concept: https://dkrz-sw.gitlab-pages.dkrz.de/yaxt/da/d3c/concept.html
* index generating functions:  https://dkrz-sw.gitlab-pages.dkrz.de/yaxt/d4/d5e/idxlists.html
* the ideas come from the Yet Another Tutorial(YAT): https://gitlab.dkrz.de/k202077/yat
